from bisect import bisect_left
from collections.abc import Sequence, Set
from itertools import chain

"""
The index() method of sequence protocol will be inherited from Sequence
module in the collections.abc package 
"""


class SortedSet(Sequence, Set):
    def __init__(self, items=None):
        self._items = sorted(set(items)) if items is not None else []

    # Container Protocol
    def __contains__(self, item):
        try:
            self.index(item)
            return True
        except ValueError:
            return False

    # Sized Protocol
    def __len__(self):
        return len(self._items)

    # Iterable Protocol
    def __iter__(self):
        return iter(self._items)

    # Sequence Protocol
    def __getitem__(self, index):
        result = self._items[index]
        return SortedSet(result) if isinstance(index, slice) else result

    # repr Protocol
    def __repr__(self):
        return "SortedSet({})".format(
            repr(self._items) if self._items else ''
        )

    # Equality and NotEquality Protocol
    def __eq__(self, rhs):
        if not isinstance(rhs, SortedSet):
            # using `return` not `raise`
            return NotImplemented
        return self._items == rhs._items

    def __ne__(self, rhs):
        if not isinstance(rhs, SortedSet):
            # using `return` not `raise`
            return NotImplemented
        return self._items != rhs._items

    def _is_unique_and_sorted(self):
        return all(self[i] < self[i + 1] for i in range(len(self) - 1))

    def count(self, item):
        assert self._is_unique_and_sorted()
        return int(item in self)

    def __add__(self, rhs):
        return SortedSet(chain(self._items, rhs._items))

    # the sortedSet must be on the left hand side
    def __mul__(self, rhs):
        return self if rhs > 0 else SortedSet()

    def __rmul__(self, lhs):
        return self * lhs

    def issubset(self, iterable):
        return self <= SortedSet(iterable)

    def issuperset(self, iterable):
        return self >= SortedSet(iterable)

    def intersection(self, iterable):
        return self & SortedSet(iterable)

    def union(self, iterable):
        return self | SortedSet(iterable)

    def symmetric_difference(self, iterable):
        return self ^ SortedSet(iterable)

    def difference(self, iterable):
        return self - SortedSet(iterable)