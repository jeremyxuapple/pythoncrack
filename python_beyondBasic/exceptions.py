import io
import math
import sys
import traceback


def median(iterable):
    """Obtain the central value of a series.

    Sorts the iterable and returns the middle value if there is an even
    number of elements, or the arithmetic mean of the middle two elements
    if there is an even number of elements.

    Args:
        iterable: A series of orderable items.

    Returns:
        The median value.
    """
    items = sorted(iterable)
    if len(items) == 0:
        # Exception payload is the specify message passed in to the exception object
        raise ValueError("median() arg is an empty sequence")
    median_index = (len(items) - 1) // 2
    if len(items) % 2 != 0:
        return items[median_index]
    return (items[median_index] + items[median_index + 1]) / 2.0


class TriangleError(Exception):

    def __init__(self, text, sides):
        super().__init__(text)
        self._sides = sides

    # @property
    # def sides(self):
    #     return self._sides

    def __repr__(self):
        return "TriangleError: {!r}, {!r}".format(self.args[0], self._sides)

    def __str__(self):
        return "'{}' for sides {}".format(self.args[0], self._sides)


def traiangle_area(a, b, c):
    sides = sorted((a, b, c))
    if sides[2] > sides[0] + sides[1]:
        raise TriangleError('Illegal Triangle', sides)
    p = (a + b + c) / 2
    a = math.sqrt(p * (p - a) * (p - b) * (p - c))
    return a


class InclinationError(Exception):
    pass


def inclination(dx, dy):
    try:
        return math.degrees(math.atan(dy / dx))
    except ZeroDivisionError as e:
        raise InclinationError('Slope can not be vertical') from e


def modules_three(n):
    r = n % 3
    if r == 0:
        print("Mutiple of 3")
    elif r == 1:
        print('Remainder is 1')
    else:
        # assert statement, 'the message for the assertation'
        assert r == 2, "Reminder is not 2"
        print('Remainder is 2')


def main():
    try:
        median([])
    except ValueError as e:
        print("Payload:", e.args)
        print(str(e))

    try:
        b'\x81'.decode('utf-8')
    except UnicodeError as e:
        print(e.args)
        print("encoding:", e.encoding)
        print("reason:", e.reason)
        print("object:", e.object)
        print("start:", e.start)
        print("end", e.end)

    # Chaining Exceptions
    try:
        traiangle_area(2, 4, 9)
    except TriangleError as e:
        try:
            print(e, file=sys.stdin)
        except io.UnsupportedOperation as ioError:
            print(e)
            print(ioError)
            print(ioError.__context__ is e)

    # explicit chaining
    try:
        inclination(0, 9)
    except InclinationError as e:
        print(e)
        # the __cause__ will show the chaining Exception
        print(e.__cause__)
        print(e.__traceback__)
        traceback.print_tb(e.__traceback__)

        # Prefer to render __trace_back__ to a string
        s = traceback.format_tb(e.__traceback__)
        print(s)

    modules_three(4)


if __name__ == '__main__':
    main()
