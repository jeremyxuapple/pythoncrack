"""
Scoping rules
LEGB: Local, Enclosing, Global, Build-in

Functions can be treated as any other objects: First Class Functions
"""

message = 'global'

def enclosing():
    message = 'enclosing'

    def local():
        nonlocal message
        # global message
        message = 'local'

    print('enclosing message:', message)
    local()
    print('enclosing message:', message)

print('global message', message)
enclosing()
print('global message', message)


"""
Keyword 'nonlocal' will bind the 'message' variable with enclosure function scope
Keyword 'global' will bind the 'message' vairable with global scope
"""

import time

def make_timer():
    last_called = None
    readableTime = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))
    print('Timer made at:', readableTime)
    def elapsed():
        nonlocal last_called
        # Every time the elapsed got called will rebind the last_called vairable
        now = time.time()
        if last_called == None:
            last_called = now
            return None
        result = now - last_called
        last_called = now
        return result

    return elapsed

"""
Decorator: accepts callable and return callable
"""
def timeStamp(f):
    def wrap(*args, **kwargs):
        x = f(*args, **kwargs)
        print(time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time())))

    return wrap

@timeStamp
def say_hi():
    print('hello world!')

# Use class as the decorator
class call_count:
    def __init__(self, f):
        self.f = f
        self.count = 0

    def __call__(self, *args, **kwargs):
        self.count += 1
        return self.f(*args, **kwargs)

@call_count
def hello(initiator):
    print("hello from {}".format(initiator))

# >>> in the python console, we can use hello.count to see how many times the function has been called
# the class decorator has been able to update the function object with more attributes


#Use the instance of a class to be as the decorator
class tracer:
    def __init__(self):
        self.enabled = True

    def __call__(self, f):
        def wrap(*args, **kwargs):
            if self.enabled:
                print("Calling function {}". format(f))
            return f(*args, **kwargs)
        return wrap

"""
Multiple Decorators can be used and they will be called in the reversed orders
"""

"""
Use functools.wraps(f) to inheritance the metadata from the original fucntion when
using the decorator
"""
import functools

def noop(f):
    @functools.wraps(f)
    def wraper(f):
        return f()
    return wrapper

@noop
def hello():
    print('Hello, world!')

"""
The check_for_non_negative is not the decorator, the validator got returned is the actually decorator function
"""
def check_for_non_negative(index):
    def validator(f):
        def wrapper(*args):
            if args[index] < 0:
                raise ValueError("The parameter at {} must be a positive number".format(*args[index]))
            return f(*args)
        return wraper
    return validator

@check_for_non_negative(1)
def calculate_size(width, length):
    return width * length
