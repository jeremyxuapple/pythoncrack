import contextlib
import sys


class LoggingContextManager:
    """
    contextManager protocol requires two methods __enter__() and __exit__()

    """
    def __enter__(self):
        print('__enter__() method from LoggingContextManager')
        return 'You are in the with block'

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            print('No exception is detected. LoggingContextManager __exit__ normally')
        else:
            print('LoggingContextManager __exit__() ({}, {}, {})'.format(exc_type, exc_val, exc_tb))
        # By default the __exit__ from ContextManager will propagate the exception from the with block,
        # since the default of the function will return None which will be evaluated as False
        """
        with LoggingContextManager() as L:
            raise ValueError('something went wrong')
        """
        # But if we explicit return True, the exception in the with-block will not be propagated
        return True


"""
A generator is a function using yield instead of return
"""


@contextlib.contextmanager
def my_context_manager():
    # START
    print('You are entering the context manager')

    try:
        # yield will be same as return in the __enter__()
        yield 'You are in the with-block'
        return 'Normal exit'
    except Exception:
        print('Exception detected', sys.exc_info())
        # if the exception in the with-block needs to be propagate, use the raise keyword
        # or the exception will be swallowed
        raise


class Connection:
    def __init__(self):
        self.xid = 0

    def _start_transaction(self):
        print('starting transaction', self.xid)
        self.xid += 1
        return self.xid

    def _commit_transaction(self, xid):
        print('committing transaction', xid)

    def _rollback_transaction(self, xid):
        print('rolling back transaction', xid)


class Transaction:
    def __init__(self, conn):
        self.conn = conn
        self.xid = conn._start_transaction()

    def commit(self):
        self.conn._commit_transaction(self.xid)

    def rollback(self):
        self.conn._rollback_transaction(self.xid)


@contextlib.contextmanager
def start_transaction(connection):
    tx = Transaction(connection)

    try:
        yield tx
    except Exception:
        tx.rollback()
        raise

    tx.commit()

"""
conn = Connection()

try:
    with start_transaction(conn) as tx:
        x = 1
        raise ValueError()
        y = x + 2
        print('transaction 0 = ', x, y)
except ValueError:
    print('transaction 0 should be failed!')

>>>starting transaction 0
>>>rolling back transaction 0
>>>transaction 0 should be failed

try:
    with start_transaction(conn) as tx:
        x = 1 + 1
        y = x + 2
        print('transaction 1 = ', x, y)
except ValueError:
    assert False

>>>starting transaction 1
>>>transaction 1 = 2, 4
>>>committing transaction 1

"""