class SimpleList:
    def __init__(self, items):
        self._items = list(items)

    def add(self, item):
        self._items.append(item)

    def __getitem__(self, index):
        return self._items[index]

    def sort(self):
        self._items.sort()

    def __len__(self):
        return len(self._items)

    def __repr__(self):
        return "SimpleList({!r})".format(self._items)


class SortedList(SimpleList):
    def __init__(self, items=()):
        super().__init__(items)
        self.sort()

    def add(self, item):
        super().add(item)
        self.sort()

    def __repr__(self):
        return "SortedList({!r})".format(list(self))


class IntList(SimpleList):
    def __init__(self, items=()):
        for x in items: self._validate(x)
        super().__init__(items)

    def add(self, item):
        self._validate(item)
        super().add(item)

    @staticmethod
    def _validate(x):
        if not isinstance(x, int):
            raise TypeError('item must be an integer to create the instance')

    def __repr__(self):
        return "IntList({!r})".format(list(self))


class SortedIntList(IntList, SortedList):
    def __repr__(self):
        return "SortedIntList({!r})".format(list(self))


"""
class-bound proxy:
super(base-class, derived-class)
>>>super(SortedList, SortedIntList).add
this will look up starting from the base class of the first argument(SortedList) which is SimpleList
<function SimpleList.add at 0X10643a050>
and the proxy is bound to the class, not to the instance
>>>super(SortedList, SortedIntList)._validate('hello')
instance-bound proxy:
super(class, instance-of-classs)
 

"""