points = [(x, y) for x in range(0, 5) for y in range(0, 5)]

points_for_loop = []

for x in range(0, 5):
    for y in range(0, 5):
        points_for_loop.append((x, y))

values = [x / (x-y)
          for x in range(1, 100)
          if x > 50
          for y in range(0, 100)
          if x - y != 0]

values_for_loop = []
for x in range(1, 100):
    if x > 50:
        for y in range(0, 100):
            if x - y != 0:
                values_for_loop.append(x / (x - y))

values = [[y * 3 for y in range(x)] for x in range(10)]

outer = []
for x in range(10):
    inner = []
    for y in range(x):
        inner.append(y * 3)
    outer.append(inner)

# map is lazy - it only produces values when it needs
# and it returns an iterable object
ordNum = map(ord, 'howdy jeremy!')

# map can receive multiple sequences, it will take the elements inside of each sequence
# and apply the function in corresponding orders, and will terminate with the first sequece
# which runs out the elements

filter(lambda x: x > 0, [1, -4, 9])


"""
reduce:
"""
from functools import reduce
import operator

reduce(operator.add, [1, 2, 3, 4, 5])

numbers = [1, 2, 3, 4, 5]
accumulator = numbers[0] + numbers[1]

for item in numbers[2:]:
    operator.add(item, accumulator)


def mul(x, y):
    print("mul {} * {}".format(x, y))
    return x * y


reduce(mul, range(1, 10))
"""
mul 1 * 2
mul 2 * 3
mul 6 * 4
mul 24 * 5
mul 120 * 6
mul 720 * 7
mul 5040 * 8
mul 40320 * 9
362880
"""

documents = [
    'It was the best of times, it was the worst of times.',
    'I went to the woods because I wished to live deliberately, to front only the essential facts of life...',
    'Friends, Romans, countrymen, lend me your ears; I come to bury Caesar, not to praise him.',
    'I do not like green eggs and ham. I do not like them, Sam-I-Am.'
]


def count_word(doc):
    normalized_doc = ''.join(c.lower() if c.isalpha() else ' ' for c in doc)
    # isalpha(): to verify if the character is an alphabet, if the character is alphabet
    # change it to lower case, if not use ' ' to append in to normalized_doc
    frequencies = {}
    for word in normalized_doc.split():
        frequencies[word] = frequencies.get(word, 0) + 1
    return frequencies


counts = map(count_word, documents)


def combine_counts(d1, d2):
    d = d1.copy()
    for word, count in d2.items():
        d[word] = d.get(word, 0) + count
    return d

# reduce(combine_counts, counts): map_reduce
"""
reduce
functools.reduce()
repeatedly apply a fucntion to a sequence, reducing them into a single value 
"""

class ExampleIterable:
    def __init__(self, data):
        self.index = 0
        self.data = data

    def __iter__(self):
        return self

    def __next__(self):
        if self.index >= len(self.data):
            raise StopIteration

        result = self.data[self.index]
        self.index += 1
        return result


class ExampleIterator:
    def __init__(self):
        self.data = [1, 2, 3]

    def __iter__(self):
        return ExampleIterable(self.data)

"""
Extended iter()
iter(callable, sentinel)

callable: callable object takes zero arguments
sentinel: (guard)Iteration stops when callable produces this value


You should
see this
text.
END
But not
this text.

"""

# with open('ending_file.txt', 'rt') as f:
#     for line in iter(lambda: f.readline().strip(), 'END'):
#         print(line)

import datetime
import random
import itertools
import time

class Sensor:
    def __iter__(self):
        return self

    def __next__(self):
        return random.random()

sensor = Sensor()
timestamps = iter(datetime.datetime.now, None)
# as datetime.datetime.now will never return None, timestamps will be a infinite iterator
for stamp, value in itertools.islice(zip(timestamps, sensor), 10):
    print(stamp, value)
    time.sleep(1)