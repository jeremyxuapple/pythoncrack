class point2d:
    def __init__(self, x, y):
        self._x = x
        self._y = y

    def __str__(self):
        """
        >>>p = point2d(2,4)
        >>>str(p)
        this build-in str() function will invoke the __str__ function, so we can override
        it if needed.
        """
        return ("{}, {}".format(self._x, self._y))

    def __repr__(self):
        """
        >>>p = point2d(2,4)
        >>>str(p)
        this build-in repr() function will invoke the __repr__ function, so we can override
        it if needed.
        """
        return ("Point2D(x={}, y={})".format(self._x, self._y))

    def __format__(self, f):
        """
        Return a string representation of self formatted according to the format
        which spcifier f
        "{:xyz}.format()"
        {field_name: field_value}
        >>>"this is a formatted point: {}". format(point2d(3,2))
        >>>'this is a formatted point: [Formatted Point2d is 3, 2, ]'
        >>> "this is a formatted point {:r}".format(point2d(2,4))

        >>>"{!r}.format(point2d(2,4))" -> force to use repr() to print the object which is default
        using str()
        "{!s}.format(point2d(2,4))"
        """
        if f == 'r':
            return "[Formatted Point2d is {}, {}, {}]".format(self._y, self._x, f)
        else:
            return "[Formatted Point2d is {}, {}, {}]".format(self._x, self._y, f)
