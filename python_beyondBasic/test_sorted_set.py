import unittest
from collections.abc import (Container, Sized, Iterable, Sequence, Set)
from sorted_set import SortedSet


class TestConstruction(unittest.TestCase):

    def test_empty(self):
        s = SortedSet([])

    def test_from_sequence(self):
        s = SortedSet([7, 8, 1, 3])

    def test_with_duplicate(self):
        s = SortedSet([6, 6, 6])

    def test_from_iterable(self):
        def gen1234():
            yield 1
            yield 2
            yield 3
            yield 4
        g = gen1234()
        s = SortedSet()

    def test_default_empty(self):
        """
        Construction Convention: collection_from_iterable = Collection(iterable)
                                 empty_collection = Collection()
        """
        s = SortedSet()


class TestContainerProtocol(unittest.TestCase):
    def setUp(self):
        self.s = SortedSet([1, -9, 3, 8])

    def test_positive_contained(self):
        self.assertTrue(1 in self.s)

    def test_positive_not_contained(self):
        self.assertFalse(6 in self.s)

    def test_negative_contained(self):
        self.assertTrue(-9 in self.s)

    def test_negative_not_contained(self):
        self.assertFalse(-4 in self.s)


class TestSizedProtocol(unittest.TestCase):
    def test_empty(self):
        s = SortedSet()
        self.assertEqual(0, len(s))

    def test_duplicates(self):
        s = SortedSet([9, 9, 9])
        self.assertEqual(1, len(s))

    def test_one(self):
        s = SortedSet([9])
        self.assertEqual(1, len(s))

    def test_ten(self):
        s = SortedSet(range(10))
        self.assertEqual(10, len(s))


class TestIterableProtocol(unittest.TestCase):
    def setUp(self):
        self.s = SortedSet([33, 2, 12, 12,  52, 66])

    # The base class is a sorted set, the sequence of the generator
    # will a sorted set as well
    def test_iter(self):
        i = iter(self.s)
        self.assertEqual(2, next(i))
        self.assertEqual(12, next(i))
        self.assertEqual(33, next(i))
        self.assertEqual(52, next(i))
        self.assertEqual(66, next(i))
        self.assertRaises(StopIteration, lambda: next(i))

    def test_loop(self):
        index = 0
        expected = [2, 12, 33, 52, 66]
        for item in self.s:
            self.assertEqual(item, expected[index])
            index += 1


class TestSequenceProtocol(unittest.TestCase):
    def setUp(self):
        self.s = SortedSet([1, 21, 23, 4, 6, 76])

    def test_index_zero(self):
        self.assertEqual(self.s[0], 1)

    def test_index_five(self):
        self.assertEqual(self.s[5], 76)

    def test_one_beyond_the_end(self):
        """
        assertRaises is a context manager

        class OpenFile():

        def __init__(self, file_name, mode):
            self._file_name = file_name
            self._mode = mode

        def __enter__(self):
            self.file = open(self._file_name, self._mode)
            return self.file

        def __exit__(self, exc_type, exc_val, exc_tb):
            # exc_tb: traceback
            self.file.close()


        with OpenFile('sample.text', 'w') as f:
            f.write('Testing')

        """
        self.assertRaises(IndexError, lambda: self.s[6])

    def test_index_minus_one(self):
        self.assertEqual(self.s[-1], 76)

    def test_index_minus_six(self):
        self.assertEqual(self.s[-6], 1)

    def test_index_minus_beyond_start(self):
        self.assertRaises(IndexError, lambda: self.s[-7])

    def test_slice_from_start(self):
        # getting items until index=3, but exclusive
        """
        without the equality protocol implemented, the comparsion
        will be using the default equality protocol check the reference
        instead of the value of the self._items

        but list[] comparison has been changed, so we can implement the equality
        using list comparison
        :return:
        """
        self.assertEqual(self.s[:3], SortedSet([1, 4, 6]))

    def test_slice_from_end(self):
        # getting the items after index=3
        self.assertEqual(self.s[3:], SortedSet([21, 23, 76]))

    def test_slice_arbitrary(self):
        # getting items bewteen index=1 and index=3(exclusive)
        self.assertEqual(self.s[1:3], SortedSet([4, 6]))

    def test_slice_empty(self):
        self.assertEqual(self.s[10:], SortedSet())

    def test_slice_full(self):
        self.assertEqual(self.s[:], self.s)

    def test_reverse(self):
        s = SortedSet([1, 2, 4, 7])
        r = reversed(s)
        # implementing __reverse__() by default but will
        # falls to __getitem__() and __len__()
        self.assertEqual(next(r), 7)
        self.assertEqual(next(r), 4)
        self.assertEqual(next(r), 2)
        self.assertEqual(next(r), 1)
        self.assertRaises(StopIteration, lambda: next(r))

    def test_index_positive(self):
        s = SortedSet([1, 5, 8, 9])
        self.assertEqual(s.index(8), 2)

    def test_index_negative(self):
        s = SortedSet([1, 5, 8, 9])
        self.assertRaises(ValueError, lambda: s.index(11))

    # Set count will only return 0 or 1
    def test_count_zero(self):
        s = SortedSet([1, 3, 5, 66])
        self.assertEqual(s.count(2), 0)

    def test_count_one(self):
        s = SortedSet([22, 2, 22, 1])
        self.assertEqual(s.count(22), 1)

    def test_concatenate_disjoint(self):
        s = SortedSet([1, 2, 6])
        t = SortedSet([12, 23, 43])
        self.assertEquals(s + t, SortedSet([1, 2, 6, 12, 23, 43]))

    def test_concatenate_equal(self):
        s = SortedSet([1, 4, 6])
        self.assertEquals(s + s, s)

    def test_concatenate_intersect(self):
        s = SortedSet([1, 3, 5])
        t = SortedSet([3, 5, 7])
        self.assertEquals(s + t, SortedSet([1, 3, 5, 7]))

    # because the SortedSet is not mutable
    def test_repetition_zero(self):
        s = SortedSet([4, 5, 6])
        self.assertEquals(s * 0, SortedSet())

    def test_repetition_nonzero(self):
        s = SortedSet([4, 5, 6])
        self.assertEquals(s * 100, s)


class TestReprProtocol(unittest.TestCase):
    def testEmpty(self):
        s = SortedSet()
        self.assertEqual(repr(s), "SortedSet()")

    def test_repr_some(self):
        s = SortedSet([12, 31, 1])
        self.assertEqual(repr(s), "SortedSet([1, 12, 31])")


class TestEqualityProtocol(unittest.TestCase):
    def test_positive_equal(self):
        self.assertTrue(SortedSet([1, 2, 32]) == SortedSet([32, 1, 2]))

    def test_negative_equal(self):
        self.assertFalse(SortedSet([21, 1, 3]) == SortedSet([15, 21]))

    def test_type_mismatch(self):
        self.assertFalse(SortedSet([2, 31]) == [2, 31])

    def test_identical(self):
        s = SortedSet([1, 2, 3])
        self.assertTrue(s == s)


class TestInequalityProtocol(unittest.TestCase):

    def test_postive_unequal(self):
        self.assertTrue(SortedSet([1, 2, 3]) != SortedSet([2, 3, 4]))

    def test_negative_unequal(self):
        self.assertFalse(SortedSet([4, 5, 6]) != SortedSet([6, 5, 4]))

    def test_type_mismatch(self):
        self.assertTrue(SortedSet([1, 2, 3]) != [1, 2, 3])

    def test_identical(self):
        s = SortedSet([1, 2, 4])
        self.assertFalse(s != s)


class TestRelationalSetProtocol(unittest.TestCase):
    """
    The relational set comparison is to used for verifying
    the left-side item is the subset of the right-side item or not

    """
    def test_lt_positive(self):
        s = {1, 2, 3}
        t = {1, 2}
        self.assertTrue(t < s)

    def test_lt_negative(self):
        s = {1, 2, 3}
        t = {1, 2}
        self.assertFalse(s < t)

    def test_le_lt_positive(self):
        s = SortedSet({1, 2})
        t = SortedSet({1, 2, 3})
        self.assertTrue(s <= t)

    def test_le_eq_positive(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({1, 2, 3})
        self.assertTrue(s <= t)

    def test_le_negative(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({1, 2})
        self.assertFalse(s <= t)

    def test_gt_positive(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({1, 2})
        self.assertTrue(s > t)

    def test_gt_negative(self):
        s = SortedSet({1, 2})
        t = SortedSet({1, 2, 3})
        self.assertFalse(s > t)

    def test_ge_gt_positive(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({1, 2})
        self.assertTrue(s > t)

    def test_ge_eq_positive(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({1, 2, 3})
        self.assertTrue(s >= t)

    def test_ge_negative(self):
        s = SortedSet({1, 2})
        t = SortedSet({1, 2, 3})
        self.assertFalse(s >= t)


class TestSetRelationMethods(unittest.TestCase):

    def test_issubset_proper_positive(self):
        s = SortedSet({1, 2})
        t = [1, 2, 3]
        self.assertTrue(s.issubset(t))

    def test_issubset_positive(self):
        s = SortedSet({1, 2, 3})
        t = [1, 2, 3, 4]
        self.assertTrue(s.issubset(t))

    def test_issuperset_negative(self):
        s = SortedSet({1, 2})
        t = [1, 2, 3]
        self.assertFalse(s.issuperset(t))


class TestOperationSetProtocol(unittest.TestCase):

    def test_intersection(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({2, 3, 4})
        self.assertEquals(s & t, SortedSet({2, 3}))

    def test_union(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({2, 3, 4})
        self.assertEquals(s | t, SortedSet({1, 2, 3, 4}))

    def test_symmetric_difference(self):
        s = SortedSet({1, 2, 3})
        t = SortedSet({2, 3, 4})
        self.assertEquals(s ^ t, SortedSet({1, 4}))

    def test_difference(self):
        """
        keep the item only exist in the first operand set
        :return:
        """
        s = SortedSet({1, 2, 3})
        t = SortedSet({2, 3, 4})
        self.assertEquals(s - t, SortedSet({1}))


class TestOperationsMethods(unittest.TestCase):

    def test_intersection(self):
        s = SortedSet({1, 2, 3})
        t = [2, 3, 4]
        self.assertEqual(s.intersection(t), SortedSet({2, 3}))

    def test_union(self):
        s = SortedSet({1, 2, 3})
        t = [2, 3, 4]
        self.assertEqual(s.union(t), SortedSet({1, 2, 3, 4}))

    def test_symmetric_difference(self):
        s = SortedSet({1, 2, 3})
        t = [2, 3, 4]
        self.assertEqual(s.symmetric_difference(t), SortedSet({1, 4}))

    def test_difference(self):
        s = SortedSet({1, 2, 3})
        t = [2, 3, 4]
        self.assertEqual(s.difference(t), SortedSet({1}))

    """
    isdisjoint(): to verify if there exist any same elements
    between two sets
    """
    def test_isdisjoint_positive(self):
        s = SortedSet({1, 2, 3})
        t = [4, 5, 6]
        self.assertTrue(s.isdisjoint(t))

    def test_isdisjoint_negative(self):
        s = SortedSet({1, 2, 3})
        t = [3, 4, 5]
        self.assertFalse(s.isdisjoint(t))


class TestSetProtocol(unittest.TestCase):

    def test_protocol(self):
        self.assertTrue(issubclass(SortedSet, Set))


if __name__ == '__main__':
    unittest.main()
