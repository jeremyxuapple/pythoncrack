# Decimal
#>>>from decmial import Decmial
#>>>decimal.getcontext().prec = 3
#>>>

#from fractions import Fraction
#numerator / denominator
#Fraction(Decimal('0.1'))
#Fraction(1,10)

"""
Complex Numbers:
Pyhton uses the electircal engineering notation for imaginary numbers
>>>complex(3)
>>>(3+0j)
>>>complex(-2,3)
>>>(-2+3j)
"""

abs(-1)
abs(Decimal(-9))
round(1.6)

import datetime
d = datetime.date.today()
year = d.year
day = d.day
# weekday() 0 -> Monday, 1 -> Tuesday
weekday = d.weekday()
# isoweekday 1 -> Monday, 2 -> Tuesday
isoweekday = d.isoweekday()
# isformat() -> 2018-08-29
isoformat = d.isoformat()

"""
>>> d = datetime.date.today()
>>> "{date:%A} {date.day} {date:%B} {date.year}".format(date=d)
'Wednesday 29 August 2018'
>>> d.strftime('%A %d %B %Y')
'Wednesday 29 August 2018'

"""
# Timedelta can be used to do math for datetime object
a = datetime.datetime(year=2014, month=3, day=12, hour=12, minute=32, second=22)
b = datetime.datetime(year=2014, month=2, day=12, hour=12, minute=32, second=22)
timeDelta = a - b
# result is: datetime.timedelta(55, 7980)
timeDelta.total_seconds()
# result is: the time difference between these two time in seconds
datetime.date.today() + datetime.timedelta(weeks=1) * 3
# timedelta() can only be used in days math, but not with the time math