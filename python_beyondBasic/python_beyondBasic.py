""" import from sys.path """
# DO NOT TYPE SPACE IN THE COMMAND in the terminal
# export PYTHONPATH=recaman

import sys

sys.path.append('recman')

sys.path.extend(['path1', 'path2'])

"""
Define the function as an object
>>>seq = sequence(immutable=True)
>>>t = seq('this is a tuple')
>>>t
('t', 'h', 'i', 's', 'i', 's', 'a', 't', 'u', 'p', 'l', 'e')
"""
def sequnece(immutable):
    if immutable:
        cls = tuple
    else:
        cls = list
    return cls


"""Conditional expression
    result = true_value if condition else false_value
"""
Friday = True
right_now = '4:23'
leave = right_now if Friday else 6


"""
*args syntax
>>> hypervolume(1, 3, 4)
>>> hypervolume(2, 4)
"""
# this will raise TypeError Exception when there is not argument passed in which is
# the correct way
def hypervolume(length, *lengths):
    v = length
    for item in lengths:
        v *= item
    return v

# this will cause Stopiteration Exception, when there is no argument passed in
def hypervolume(*lengths):
    i = iter(lengths)
    v = next(i)
    for items in i:
        v *= items
    return v


"""
Extended argument

>>>tag('img', src='google.ca', width='100px')
the double artisk makes the function being able to accept more arguments, don't use
quotes to avoid they are accepting as the positional arguments
 """
def tag(name, **attributes):
    result = "<" + name
    for key, value in attributes.items():
        result += " {k}={v}".format(k=key, v=str(value))
    result += ">"

    return result

def print_arg(arg1, arg2, *args, kwarg1, kwarg2, **kwargs):
    print(arg1)
    print(arg2)
    print(args)
    print(kwarg1)
    print(kwarg2)
    print(kwargs)

"""
>>>print_tag(1, 2, 3, 4, 5, kwarg1=9, kwarg2=10, kwarg3=90, kwarg4=88)
"""

def color(red, green, blue, **kwargs):
    print('red=', red)
    print('green=', green)
    print('blue=', blue)
    print(kwargs)

"""
>>>colors = dict(red=123, green=123, blue=112, cyan=312)
>>color(**colors)
"""

Monday = [12, 42, 23, 54, 65, 75]
Tuesday = [12, 42, 23, 54, 65, 75]
Sunday = [12, 42, 23, 54, 65, 75]

daily = [Monday, Tuesday, Sunday]

for items in zip(daily[0], daily[1], daily[2]):
    print(items)
# Both for loop will work the same
for items in zip(*daily):
    print(items)