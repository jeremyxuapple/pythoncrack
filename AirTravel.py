class Flight:
    def __init__(self, number, aircraft):
        if not number[:2].isalpha():
            raise ValueError("No airline code in '{}'".format(number))

        if not number[:2].isupper():
            raise ValueError("Invalid airline code '{}'".format(number))

        if not (number[2:].isdigit() and int(number[2:]) <= 9999):
            raise ValueError("Invalid route number '{}'".format(number))

        self._number = number
        self._aircraft = aircraft
        rows, seats = self._aircraft.seating_plan()
        self._seats_arrangement = [None] + [{letter: None for letter in seats} for _ in rows]

    def number(self):
        return self._number

    def airline(self):
        return self._number[:2]

    def aircraft_model(self):
        return self._aircraft.model()

    def seat_available(self):
        return sum(sum(1 for s in row.values() if s is None) for row in self._seats_arrangement if row is not None)

    def parse_seat(self, seat):
        rows, seat_letters = self._aircraft.seating_plan()
        letter = seat[-1]
        if letter not in seat_letters:
            raise ValueError("Invalid seat letter {}".format(letter))

        row_text = seat[:-1]
        try:
            row = int(row_text)
        except ValueError:
            raise ValueError("Invalid seat number {}".format(row_text))

        if row not in rows:
            raise ValueError("Invalid row number {}".format(row))

        return row, letter

    def allocate_seat(self, seat, passanger):
        rows, seat_letters = self._aircraft.seating_plan()

        row, letter = self.parse_seat(seat)
        if self._seats_arrangement[row][letter] is not None:
            raise ValueError("Seat {} is already taken".format(seat))

        self._seats_arrangement[row][letter] = passanger

    def relocate_seat(self, from_seat, to_seat):
        from_row, from_letter = self.parse_seat(from_seat)

        if self._seats_arrangement[from_row][from_letter] is None:
            raise ValueError("{} is not occupied yet".format(from_seat))

        to_row, to_letter = self.parse_seat(to_seat)

        if self._seats_arrangement[to_row][to_letter] is not None:
            raise ValueError("{} is already occupied".format(to_seat))

        self._seats_arrangement[to_row][to_letter] = self._seats_arrangement[from_row][from_letter]
        self._seats_arrangement[from_row][from_letter] = None


class Aircraft:

    def __init__(self, registration, model, num_rows, num_seats_per_row):
        self._registration = registration
        self._model = model
        self._num_rows = num_rows
        self._num_seats_per_row = num_seats_per_row

    def registration(self):
        return self._registration

    def model(self):
        return self._model

    def seating_plan(self):
        return (range(1, self._num_rows + 1), "ABCDEFGHIJK"[:self._num_seats_per_row])

class Boeing777(Aircraft):
    pass

class AirbusA319(Aircraft):
    pass

def make_flights():
    f = Flight('DK997', Boeing777('Kustafa-9', 'Boeing888', num_rows=55, num_seats_per_row=6))
    f.allocate_seat('18A', 'Bowing Wanring')

    g = Flight('KP886', AirbusA319('Kuston-7', 'AirbusA319', num_rows=66, num_seats_per_row=8))
    g.allocate_seat('14D', 'Wuck Duck')

    return f, g