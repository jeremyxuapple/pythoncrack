""" Generate Recaman Sequence

    a(0) = 0,
    if n > 0 and the number is not
       already included in the sequence,
         a(n) = a(n - 1) - n
    else
         a(n) = a(n-1) + n.
"""

def recaman(n):

    seen = []
    a = 0
    for index in range(1, n+1):
        c = a - index
        if c <= 0 or c in seen:
            c = a + index
        a = c
        seen.append(a)

    return seen
